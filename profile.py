#!/usr/bin/python

"""TTest profile based on ota_srslte. his profile allows the allocation of resources for over-the-air operation on
the POWDER platform. Specifically, the profile has options to request the
allocation of software defined radios (SDRs) in rooftop base-stations and
fixed-endpoints (i.e., nodes deployed at human height).

This profile uses a disk image with srsLTE and UHD pre-installed, with an option
for including the srsLTE source code.

Resources needed to realize a basic srsLTE setup consisting of a UE, an eNodeB
and an EPC core network:

  * Spectrum for LTE FDD opperation (uplink and downlink).
  * A "nuc1" fixed-endpoint compute/SDR pair (This will run the UE side.)
  * A "cbrssdr" base station SDR. (This will be the radio side of the eNodeB.)
  * A "d740" compute node. (This will run both the eNodeB software and the EPC software.)
  
**Example resources that can be used (and that need to be reserved before
  instantiating the profile):**

  * Hardware (at least one set of resources are needed):
   * WEB, nuc1; Emulab, cbrssdr1-browning; Emulab, d740
   * Bookstore, nuc1; Emulab, cbrssdr1-browning; Emulab, d740
   * WEB, nuc1; Emulab, cbrssdr1-meb; Emulab, d740
  * Spectrum:
   * Uplink: 2500 MHz to 2510 MHz
   * Downlink: 2620 MHz to 2630 MHz

The instuctions below assume the first hardware configuration.

Instructions:

These instructions assume the following hardware set was selected when the
profile was instantiated:

 * WEB, nuc1; Emulab, cbrssdr1-browning; Emulab, d740

#### To run the srsLTE software

**To run the EPC**

Open a terminal on the `cbrssdr1-browning-comp` node in your experiment. (Go to
the "List View" in your experiment. If you have ssh keys and an ssh client
working in your setup you should be able to click on the black "ssh -p ..."
command to get a terminal. If ssh is not working in your setup, you can open a
browser shell by clicking on the Actions icon corresponding to the node and
selecting Shell from the dropdown menu.)

Start up the EPC:

    sudo srsepc
    
**To run the eNodeB**

Open another terminal on the `cbrssdr1-browning-comp` node in your experiment.

Start up the eNodeB:

    sudo srsenb

**To run the UE**

Open a terminal on the `b210-web-nuc1` node in your experiment.

Start up the UE:

    sudo srsue

**Verify functionality**

Open another terminal on the `b210-web-nuc1` node in your experiment.

Verify that the virtual network interface tun_srsue" has been created:

    ifconfig tun_srsue

Run ping to the SGi IP address via your RF link:
    
    ping 172.16.0.1

Killing/restarting the UE process will result in connectivity being interrupted/restored.

If you are using an ssh client with X11 set up, you can run the UE with the GUI
enabled to see a real time view of the signals received by the UE:

    sudo srsue --gui.enable 1

#### Troubleshooting

**No compatible RF-frontend found**

If srsenb fails with an error indicating "No compatible RF-frontend found",
you'll need to flash the appropriate firmware to the X310 and power-cycle it
using the portal UI. Run `uhd_usrp_probe` in a shell on the associated compute
node to get instructions for downloading and flashing the firmware. Use the
Action buttons in the List View tab of the UI to power cycle the appropriate
X310 after downloading and flashing the firmware. If srsue fails with a similar
error, try power-cycling the associated NUC.

**UE can't sync with eNB**

If you find that the UE cannot sync with the eNB, passing
`--phy.force_ul_amplitude 1.0` to srsue may help. You may have to rerun srsue a
few times to get it to sync.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


class GLOBALS:
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE:4"
    GR_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18-GR-PBUF"
    SRSLTE_SRC_DS = "urn:publicid:IDN+emulab.net:powderteam+imdataset+srslte-src-v19"
    


def x310_node_pair(idx, x310_radio):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("%s-comp"%(x310_radio.radio_name))
    node.hardware_type = params.x310_pair_nodetype
    node.disk_image = GLOBALS.SRSLTE_IMG
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/setup_x310.sh"))

    if params.include_srslte_src:
        bs = node.Blockstore("bs-comp-%s"%idx, "/opt/srslte")
        bs.dataset = GLOBALS.SRSLTE_SRC_DS

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310"%(x310_radio.radio_name))
    radio.component_id = x310_radio.radio_name
    radio.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    radio_link.addNode(radio)


def b210_nuc_pair(idx, b210_node):
    b210_nuc_pair_node = request.RawPC("b210-%s-%s"%(b210_node.aggregate_id,"nuc1"))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm"%(b210_node.aggregate_id)
    b210_nuc_pair_node.component_manager_id = agg_full_name
    b210_nuc_pair_node.component_id = "nuc1"
    b210_nuc_pair_node.disk_image = GLOBALS.GR_IMG
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/setup_b210.sh"))

    


portal.context.defineParameter("include_srslte_src",
                               "Include srsLTE source code.",
                               portal.ParameterType.BOOLEAN,
                               False)

node_type = [
    ("d740",
     "Emulab, d740"),
    ("d430",
     "Emulab, d430")
]

portal.context.defineParameter("x310_pair_nodetype",
                               "Type of compute node paired with the X310 Radios",
                               portal.ParameterType.STRING,
                               node_type[0],
                               node_type)

rooftop_names = [
    ("cbrssdr1-browning",
     "Emulab, cbrssdr1-browning (Browning)"),
    ("cbrssdr1-bes",
     "Emulab, cbrssdr1-bes (Behavioral)"),
    ("cbrssdr1-dentistry",
     "Emulab, cbrssdr1-dentistry (Dentistry)"),
    ("cbrssdr1-fm",
     "Emulab, cbrssdr1-fm (Friendship Manor)"),
    ("cbrssdr1-honors",
     "Emulab, cbrssdr1-honors (Honors)"),
    ("cbrssdr1-meb",
     "Emulab, cbrssdr1-meb (MEB)"),
    ("cbrssdr1-smt",
     "Emulab, cbrssdr1-smt (SMT)"),
    ("cbrssdr1-hospital",
     "Emulab, cbrssdr1-hospital (Hospital)"),
    ("cbrssdr1-ustar",
     "Emulab, cbrssdr1-ustar (USTAR)"),
]

portal.context.defineStructParameter("x310_radios", "X310 Radios", [],
                                     multiValue=True,
                                     itemDefaultValue=
                                     {},
                                     min=0, max=None,
                                     members=[
                                        portal.Parameter(
                                             "radio_name",
                                             "Rooftop base-station X310",
                                             portal.ParameterType.STRING,
                                             rooftop_names[0],
                                             rooftop_names)
                                     ])

fixed_endpoint_aggregates = [
    ("web",
     "WEB, nuc1"),
    ("bookstore",
     "Bookstore, nuc1"),
    ("humanities",
     "Humanities, nuc1"),
    ("law73",
     "Law 73, nuc1"),
    ("ebc",
     "EBC, nuc1"),
    ("madsen",
     "Madsen, nuc1"),
    ("sagepoint",
     "Sage Point, nuc1"),
    ("moran",
     "Moran, nuc1"),
    ("cpg",
     "Central Parking Garage, nuc1"),
    ("guesthouse",
     "Guest House, nuc1"),
]

portal.context.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "aggregate_id",
                                             "Fixed Endpoint B210",
                                             portal.ParameterType.STRING,
                                             fixed_endpoint_aggregates[0],
                                             fixed_endpoint_aggregates)
                                     ],
                                    )

# Frequency/spectrum parameters
portal.context.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3560.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])



params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()
for i, frange in enumerate(params.freq_ranges):
    if frange.freq_min < 3400 or frange.freq_min > 3800 \
       or frange.freq_max < 3400 or frange.freq_max > 3800:
        perr = portal.ParameterError("Frequencies must be between 3400 and 3800 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

portal.context.verifyParameters()

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

for i, x310_radio in enumerate(params.x310_radios):
    x310_node_pair(i, x310_radio)

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node)

portal.context.printRequestRSpec()
